#pragma once
#include <math.h>
typedef struct
{
	double x;
	double y;	
}Vector2d;

inline double absVector(Vector2d r)
{
    return sqrt(r.x*r.x + r.y*r.y);
}