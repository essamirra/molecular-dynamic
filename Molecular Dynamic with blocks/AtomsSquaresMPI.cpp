#include <mpi.h>
#include "AtomsSquaresMPI.h"
#include <cmath>
#include <algorithm>


void Init(AtomsSquaresMPI* s, const char* fileName, int blocksInEdge)
{
    InitSystemParameters(s, fileName, blocksInEdge);
    AllocSystemMem(s);
    InitMPISystemParameters(s);
    BuidBlocksSystem(s);
}


void InitSystemParameters(AtomsSquaresMPI* s, const char* fileName, int blocksInEdge)
{
    FILE* file = fopen(fileName, "r");
    if (file == nullptr)
    {
        printf("ERROR: Can't open file %s", fileName);
        return;
    }
    fscanf(file, "N=%u\r\n", &s->n);
    fscanf(file, "SIGMA=%lf\r\n", &s->sigma);
    fscanf(file, "EPSILON=%lf\r\n", &s->epsilon);
    fscanf(file, "DT=%lf\r\n", &s->deltaTime);
    fscanf(file, "T=%lf\r\n", &s->currentTime);
    fscanf(file, "XMIN=%lf\r\n", &s->xmin);
    fscanf(file, "XMAX=%lf\r\n", &s->xmax);
    fscanf(file, "YMIN=%lf\r\n", &s->ymin);
    fscanf(file, "YMAX=%lf\r\n", &s->ymax);
    s->height = s->ymax - s->ymin;
    s->width = s->xmax - s->xmin;
    fscanf(file, "ITERATIONS=%u\r\n", &s->maxIteration);
    s->iter = 0;
    s->blocksInEdge = blocksInEdge;
    s->blockHeight = s->height / blocksInEdge;
    s->blockWidth = s->width / blocksInEdge;
    fclose(file);
}

void AllocSystemMem(AtomsSquaresMPI* s)
{
    s->posX = new double[s->n];
    s->posY = new double[s->n];
    s->prevPosX = new double[s->n];
    s->prevPosY = new double[s->n];
    s->nextPosX = new double[s->n];
    s->nextPosY = new double[s->n];
    s->copyPosX = new double[s->n];
    s->copyPosY = new double[s->n];
    s->copyPrevPosX = new double[s->n];
    s->copyPrevPosY = new double[s->n];
    s->velX = new double[s->n];
    s->velY = new double[s->n];
    s->forceX = new double[s->n];
    s->forceY = new double[s->n];
    s->blocks = new int[s->blocksInEdge * s->blocksInEdge + 1];
}

void InitMPISystemParameters(AtomsSquaresMPI* s)
{
    MPI_Comm_rank(MPI_COMM_WORLD, &s->rank);
    MPI_Comm_size(MPI_COMM_WORLD, &s->proccesNum);
    int processesInEdge = (int)sqrt(s->proccesNum);
    int colsCount = processesInEdge, rowsCount = processesInEdge;

    int proccesCol = s->rank % colsCount;
    int proccesRow = s->rank / rowsCount;

    //������-���������!

    s->rankOfMyUpper = (s->rank + colsCount) % s->proccesNum;
    s->rankOfMyLower = (s->rank + s->proccesNum - colsCount) % s->proccesNum;

    int minInRow = colsCount * (proccesRow);

    s->rankOfMyRight = (s->rank + 1) % (colsCount) + minInRow;
    s->rankOfMyLeft = (s->rank + -1 + colsCount) % (colsCount) + minInRow;


    //����� ������� � ���� ������

    int countOfBlocksCols = s->blocksInEdge / processesInEdge;
    s->blocksInEdgeForProccess = countOfBlocksCols;
    s->myXmin = s->xmin + countOfBlocksCols * s->blockWidth * proccesCol;
    s->myYmin = s->ymin + countOfBlocksCols * s->blockHeight * proccesRow;
    s->myXmax = s->myXmin + countOfBlocksCols * s->blockWidth;
    s->myYmax = s->myYmin + countOfBlocksCols * s->blockHeight;
}

int CalcSquare(AtomsSquaresMPI * a, int id)
{
    //4op
    return (int)floor((a->posX[id] - a->xmin) / a->blockWidth) + (a->blocksInEdge)*((int)floor((a->posY[id] - a->ymin) / a->blockHeight));
}
void ReadAtomsFromFile(AtomsSquaresMPI* s, const char* fileName)
{
    FILE* file = fopen(fileName, "r");
    if (file == NULL)
    {
        printf("ERROR: Can't open file %s", fileName);
        return;
    }
    int processesInEdge = (int)sqrt(s->proccesNum);
    int colsCount = processesInEdge, rowsCount = processesInEdge;
    int proccesCol = s->rank % colsCount;
    int proccesRow = s->rank / rowsCount;
    int countOfBlocksCols = s->blocksInEdge / processesInEdge;
    int myStartBlocksCol = countOfBlocksCols * proccesCol;
    int myStartBlocksRow = countOfBlocksCols * proccesRow;
    double myLeftStartBlockCol = myStartBlocksCol - 1;
    double * tempX, *tempY, *velX, *velY;
    tempX = new double[s->n];
    tempY = new double[s->n];
    velX = new double[s->n];
    velY = new double[s->n];

    for (int i = 0; i < s->n; ++i)
    {
        fscanf(file, "%lf%lf%lf%lf",tempX[i], tempY[i], velX[i], velY[i]);     
    }
    BuildBlocksArr(s);
    for (int i = 0; i < s->n; ++i)
    {
        int atomSquare = CalcSquare(s, i);
        if(atomSquare )
    }

    fclose(file);
}

int CalcSquareMPI(AtomsSquaresMPI* a, int id)
{
    //4op
    return (int)floor((a->posX[id] - a->xmin) / a->blockWidth) + (a->blocksInEdgeForProccess) * ((int)floor((a->posY[id] - a->ymin) / a->blockHeight));
}

uint64_t BuildBlocksArr(AtomsSquaresMPI * s)
{

    for (int i = 0; i < s->blocksInEdge*s->blocksInEdge + 1; ++i)
        s->blocks[i] = 0;
    //4n
    for (int i = 0; i < s->n; ++i)
        s->blocks[CalcSquare(s, i)] += 1;


    for (int i = 1; i < s->blocksInEdge*s->blocksInEdge + 1; ++i)
        s->blocks[i] += s->blocks[i - 1];

    //4n
    for (int i = 0; i < s->n; ++i)
    {
        int blockNum = CalcSquare(s, i);
        s->blocks[blockNum] -= 1;
        int newPosition = s->blocks[blockNum];

        s->copyPosX[newPosition] = s->posX[i];
        s->copyPosY[newPosition] = s->posY[i];
        s->copyPrevPosX[newPosition] = s->prevPosX[i];
        s->copyPrevPosY[newPosition] = s->prevPosY[i];
    }

    std::swap(s->posX, s->copyPosX);
    std::swap(s->posY, s->copyPosY);
    std::swap(s->prevPosX, s->copyPrevPosX);
    std::swap(s->prevPosY, s->copyPrevPosY);
    //8n
    return 8 * s->n;
}

void BuidBlocksSystem(AtomsSquaresMPI* s)
{
    auto blocks_in_edge = s->blocksInEdgeForProccess + 2;
    for (int i = 0; i < blocks_in_edge * blocks_in_edge + 1; ++i)
        s->blocks[i] = 0;
    //4n
    for (int i = 0; i < s->n; ++i)
        s->blocks[CalcSquareMPI(s, i)] += 1;


    for (int i = 1; i < blocks_in_edge * blocks_in_edge + 1; ++i)
        s->blocks[i] += s->blocks[i - 1];

    //4n
    for (int i = 0; i < s->n; ++i)
    {
        int blockNum = CalcSquareMPI(s, i);
        s->blocks[blockNum] -= 1;
        int newPosition = s->blocks[blockNum];

        s->copyPosX[newPosition] = s->posX[i];
        s->copyPosY[newPosition] = s->posY[i];
        s->copyPrevPosX[newPosition] = s->prevPosX[i];
        s->copyPrevPosY[newPosition] = s->prevPosY[i];
    }

    std::swap(s->posX, s->copyPosX);
    std::swap(s->posY, s->copyPosY);
    std::swap(s->prevPosX, s->copyPrevPosX);
    std::swap(s->prevPosY, s->copyPrevPosY);
}

void UpdateProccesAtoms(AtomsSquaresMPI* s)
{
#pragma omp parallel for reduction(+:count)
    for (int i = 1; i < s->blocksInEdgeForProccess; i++)
    {
        for (int j = 1; j < s->blocksInEdgeForProccess; j++)
        {
            int currentBlock = i * (s->blocksInEdgeForProccess + 2) + j;
            for (int currentAtom = s->blocks[currentBlock]; currentAtom < s->blocks[currentBlock + 1]; ++currentAtom)
            {
                UpdateForce(s, currentBlock, currentAtom); //done
                if (s->iter == 0)
                    EulerStep(s, currentAtom);
                else
                    VerleStep(s, currentAtom);
            }
        }
    }
    UpdatePositions(s);
}

void NextStep(AtomsSquaresMPI* s)
{
    UpdateProccesAtoms(s);
    SendAndRecieveFlyers(s);
    BuildBlocksSystemForProccessAtoms(s);
    SendAndRecieveEdges(s);
    BuidBlocksSystem(s);
    s->currentTime += s->deltaTime;
    s->iter++;
}

void SendAndRecieveFlyers(AtomsSquaresMPI* s)
{
}

void BuildBlocksSystemForProccessAtoms(AtomsSquaresMPI* s)
{
}

void SendAndRecieveEdges(AtomsSquaresMPI* s)
{
}

void UpdateForce(AtomsSquaresMPI* s, int currentBlock, int currentAtom)
{
    s->forceX[currentAtom] = 0.0;
    s->forceY[currentAtom] = 0.0;

    int blockX = currentBlock % s->blocksInEdgeForProccess + 2;
    int blockY = currentBlock / s->blocksInEdgeForProccess + 2;

    CalcForceWithNeighbors(s, currentAtom, blockX - 1, blockY - 1);
    CalcForceWithNeighbors(s, currentAtom, blockX - 1, blockY);
    CalcForceWithNeighbors(s, currentAtom, blockX - 1, blockY + 1);
    CalcForceWithNeighbors(s, currentAtom, blockX, blockY - 1);
    CalcForceWithNeighbors(s, currentAtom, blockX, blockY);
    CalcForceWithNeighbors(s, currentAtom, blockX, blockY + 1);
    CalcForceWithNeighbors(s, currentAtom, blockX + 1, blockY - 1);
    CalcForceWithNeighbors(s, currentAtom, blockX + 1, blockY);
    CalcForceWithNeighbors(s, currentAtom, blockX + 1, blockY + 1);
}

void UpdatePositions(AtomsSquaresMPI* s)
{
    double *tempX = s->prevPosX;
    double *tempY = s->prevPosY;
    s->prevPosX = s->posX;
    s->prevPosY = s->posY;
    s->posX = s->nextPosX;
    s->posY = s->nextPosY;
    s->nextPosX = tempX;
    s->nextPosY = tempY;
}

void EulerStep(AtomsSquaresMPI* s, int currentAtom)
{
    s->prevPosX[currentAtom] = s->posX[currentAtom];
    s->prevPosY[currentAtom] = s->posY[currentAtom];

    s->nextPosX[currentAtom] = s->posX[currentAtom] +
        (s->velX[currentAtom] + s->forceX[currentAtom] * s->deltaTime) *
        s->deltaTime;
    s->nextPosY[currentAtom] = s->posY[currentAtom] +
        (s->velY[currentAtom] + s->forceY[currentAtom] * s->deltaTime) *
        s->deltaTime;
}

void VerleStep(AtomsSquaresMPI* s, int currentAtom)
{
    s->nextPosX[currentAtom] = 2 * s->posX[currentAtom] - s->prevPosX[currentAtom] +
        s->forceX[currentAtom] * s->deltaTime * s->deltaTime;
    s->nextPosY[currentAtom] = 2 * s->posY[currentAtom] - s->prevPosY[currentAtom] +
        s->forceY[currentAtom] * s->deltaTime * s->deltaTime;
}

double calculateNeighborOffset(const AtomsSquaresMPI* s, int blockCoordinate)
{
    if (blockCoordinate < 0) return -1;
    if (blockCoordinate >= s->blocksInEdgeForProccess + 2) return 1;
    else return 0;
}

void CalcForceWithNeighbors(const AtomsSquaresMPI* s, int currentAtom, int nBlockX, int nBlockY)
{
    int neighborhoodBlock = ((nBlockX + s->blocksInEdgeForProccess + 2) % (s->blocksInEdgeForProccess + 2)) + s->blocksInEdgeForProccess + 2 * ((nBlockY + s->blocksInEdgeForProccess + 2) % s->blocksInEdgeForProccess + 2);


    double offsetX = calculateNeighborOffset(s, nBlockX) * s->width;
    double offsetY = calculateNeighborOffset(s, nBlockY) * s->height;

    //(s->blocks[neighborhoodBlock + 1] - s->blocks[neighborhoodBlock]) * (36)
    for (int otherAtom = s->blocks[neighborhoodBlock]; otherAtom < s->blocks[neighborhoodBlock + 1]; ++otherAtom)
    {
        if (currentAtom == otherAtom) continue;
        double rx = s->posX[otherAtom] + offsetX - s->posX[currentAtom];
        double ry = s->posY[otherAtom] + offsetY - s->posY[currentAtom];
        double r = sqrt(rx * rx + ry * ry);
        double force = -1 * 4 * s->epsilon * (pow(s->sigma / r, 12) - pow(s->sigma / r, 6));
        s->forceX[currentAtom] += force * (rx / r);
        s->forceY[currentAtom] += force * (ry / r);
    }
}
