#pragma once
#include <cstdint>
typedef struct
{

	double sigma;
	double epsilon;
	double deltaTime;
	double currentTime;

	double width;
	double height;
	unsigned maxIteration;
	int iter;
	int n;

	double * posX;
	double * posY;
	double * prevPosX;
	double * prevPosY;
	double * nextPosX;
	double * nextPosY;
	double * velX;
	double * velY;
	double * forceX;
	double * forceY;    

	int * blocks;
	double blockWidth;
	double blockHeight;

	double * copyPosX;
	double * copyPosY;
	double * copyPrevPosX;
	double * copyPrevPosY;

	double xmin;
	double xmax;
	double ymin;
	double ymax;

}AtomsSquares;

void CalcBlockSize(AtomsSquares * a, int blockCount);
void Init(AtomsSquares * n, const char * fileName); 
int CalcSquare(AtomsSquares * a, int id);
uint64_t BuildBlocksArr(AtomsSquares * a);
void Save(AtomsSquares * n, const char * fileName); 
void Swap(double * a, double * b);
uint64_t UpdateForce(AtomsSquares * a, int currentBlock, int currentAtom);
void SortArrs(AtomsSquares * a);
uint64_t NextStep(AtomsSquares * a);
uint64_t CalcForceWithNeighbors(const AtomsSquares *s, int currentAtom, int nBlockX,
                                       int nBlockY);
void PrintData(AtomsSquares * s);
void SaveDistances(AtomsSquares * s);
uint64_t NextStepMPI(AtomsSquares * s, int procNum);

 
