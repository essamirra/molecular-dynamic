#include "AtomsSquares.h"
#include <cstdlib>
#include "math.h"
#include <stdio.h>
#include "Vector.h"
#include <algorithm>
#define BLOCKCOUNT 2
#include <iostream>
void Init(AtomsSquares * n, const char * fileName)
{
	FILE *file = fopen(fileName, "r");
	if (file == NULL)
	{
		printf("ERROR: Can't open file %s", fileName);
		return;
	}
	fscanf(file, "N=%u\r\n", &n->n);
	n->posX = new double[n->n];
	n->posY = new double[n->n];
	n->prevPosX = new double[n->n];
	n->prevPosY = new double[n->n];
	n->nextPosX = new double[n->n];
	n->nextPosY = new double[n->n];
	n->copyPosX = new double[n->n];
	n->copyPosY = new double[n->n];
	n->copyPrevPosX = new double[n->n];
	n->copyPrevPosY = new double[n->n];
	n->velX = new double[n->n];
	n->velY = new double[n->n];
	n->forceX = new double[n->n];
	n->forceY = new double[n->n];
	n->blocks = new int[BLOCKCOUNT*BLOCKCOUNT + 1];
	fscanf(file, "SIGMA=%lf\r\n", &n->sigma);
	fscanf(file, "EPSILON=%lf\r\n", &n->epsilon);
	fscanf(file, "DT=%lf\r\n", &n->deltaTime);
	fscanf(file, "T=%lf\r\n", &n->currentTime);
	fscanf(file, "XMIN=%lf\r\n", &n->xmin);
	fscanf(file, "XMAX=%lf\r\n", &n->xmax);
	fscanf(file, "YMIN=%lf\r\n", &n->ymin);
	fscanf(file, "YMAX=%lf\r\n", &n->ymax);
	n->height = n->ymax - n->ymin;
	n->width = n->xmax - n->xmin;
	fscanf(file, "ITERATIONS=%u\r\n", &n->maxIteration);
	n->iter = 0;
	double energy = 0, momentx = 0, momenty = 0;
	double maxvelocityx = 0, maxvelocityy = 0;
	for (int i = 0; i < n->n; ++i)
	{
		fscanf(file, "%lf%lf%lf%lf", &n->posX[i], &n->posY[i], &n->velX[i], &n->velY[i]);
		energy += (n->velX[i] * n->velX[i] + n->velY[i] * n->velY[i]);
		momentx += n->velX[i];
		momenty += n->velY[i];
		if (abs(n->velX[i]) > maxvelocityx)
			maxvelocityx = abs(n->velX[i]);
		if (abs(n->velY[i]) > maxvelocityy)
			maxvelocityy = abs(n->velY[i]);
	}
	std::cout << "energy " << std::fixed << energy << std::endl;
	std::cout << "momentx " << std::fixed << momentx << std::endl;
	std::cout << "momenty " << std::fixed << momenty << std::endl;
	std::cout << "max velocity x " << std::fixed << maxvelocityx << std::endl;
	std::cout << "max velocity y " << std::fixed << maxvelocityy << std::endl;
	fclose(file);

}

void Save(AtomsSquares * s, const char * fileName)
{
	FILE *file = fopen(fileName, "w+");
	if (file == NULL)
	{
		printf("ERROR: Can't create file %s", fileName);
		return;
	}

	fprintf(file, "N=%u\n", s->n);
	fprintf(file, "SIGMA=%lf\n", s->sigma);
	fprintf(file, "EPSILON=%lf\n", s->epsilon);
	fprintf(file, "DT=%lf\n", s->deltaTime);
	fprintf(file, "T=%lf\n", s->currentTime);
	fprintf(file, "XMIN=%lf\n", s->xmin);
	fprintf(file, "XMAX=%lf\n", s->xmax);
	fprintf(file, "YMIN=%lf\n", s->ymin);
	fprintf(file, "YMAX=%lf\n", s->ymax);
	fprintf(file, "ITERATIONS=%u\n", s->maxIteration);
	SortArrs(s);
	double energy = 0, momentx = 0, momenty = 0;
	double maxvelocityx = 0, maxvelocityy = 0;
	for (int i = 0; i < s->n; ++i)
	{
		Vector2d result;
		result.x = (s->posX[i] - s->prevPosX[i]) / s->deltaTime;
		result.y = (s->posY[i] - s->prevPosY[i]) / s->deltaTime;
		if (abs(result.x) > maxvelocityx)
			maxvelocityx = abs(result.x);
		if (abs(result.y) > maxvelocityy)
			maxvelocityy = abs(result.y);
		fprintf(file, "%10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf\n", s->posX[i], s->posY[i], s->prevPosX[i], s->prevPosY[i], s->forceX[i], s->forceY[i], result.x, result.y);
		energy += (result.x*result.x + result.y*result.y);
		momentx += result.x;
		momenty += result.y;
	}
	std::cout << "energy " << std::fixed << energy << std::endl;
	std::cout << "momentx " << std::fixed << momentx << std::endl;
	std::cout << "momenty " << std::fixed << momenty << std::endl;
	std::cout << "max velocity x " << std::fixed << maxvelocityx << std::endl;
	std::cout << "max velocity y " << std::fixed << maxvelocityy << std::endl;
	
	fclose(file);
}
void PrintData(AtomsSquares * s)
{
	double energy = 0, momentx = 0, momenty = 0;
	double maxvelocityx = 0, maxvelocityy = 0;
	for (int i = 0; i < s->n; ++i)
	{
		Vector2d result;
		result.x = (s->posX[i] - s->prevPosX[i]) / s->deltaTime;
		result.y = (s->posY[i] - s->prevPosY[i]) / s->deltaTime;
		if (abs(result.x) > maxvelocityx)
			maxvelocityx = abs(result.x);
		if (abs(result.y) > maxvelocityy)
			maxvelocityy = abs(result.y);		
		energy += (result.x*result.x + result.y*result.y);
		momentx += result.x;
		momenty += result.y;
	}
	std::cout << "energy " << std::fixed << energy << std::endl;
	std::cout << "momentx " << std::fixed << momentx << std::endl;
	std::cout << "momenty " << std::fixed << momenty << std::endl;
	std::cout << "max velocity x " << std::fixed << maxvelocityx << std::endl;
	std::cout << "max velocity y " << std::fixed << maxvelocityy << std::endl;

}
void SaveDistances(AtomsSquares * s)
{
	FILE *file = fopen("distances.txt", "w+");
	for (int i = 0; i < s->n; i++)
	{
		for (int j = i+1; j < s->n; j++)
		{
			double rx = s->posX[i] - s->posX[j];
			double ry = s->posY[i] - s->posY[j];
			double r = sqrt(rx * rx + ry * ry);
			fprintf(file, "%lf\n", r);
		}
	}
}
int CalcSquare(AtomsSquares * a, int id)
{
	//4op
	return (int)floor((a->posX[id] - a->xmin) / a->blockWidth) + (BLOCKCOUNT)*((int)floor((a->posY[id] - a->ymin) / a->blockHeight));
}
void SwapDouble(double &a, double &b, int count = 0)
{
	double t = a;
	a = b;
	b = t;
}
void SwapParts(AtomsSquares * a, int c, int b)
{
	SwapDouble(a->posX[c], a->posX[b]);
	SwapDouble(a->prevPosX[c], a->prevPosX[b]);
	SwapDouble(a->nextPosX[c], a->nextPosX[b]);
	SwapDouble(a->forceX[c], a->forceX[b]);

	SwapDouble(a->posY[c], a->posY[b]);
	SwapDouble(a->prevPosY[c], a->prevPosY[b]);
	SwapDouble(a->nextPosY[c], a->nextPosY[b]);
	SwapDouble(a->forceY[c], a->forceY[b]);
}
void SortArrs(AtomsSquares * a)
{
	for (int i = 0; i < BLOCKCOUNT*BLOCKCOUNT; i++)
	{
		for (int j = a->blocks[i]; j < a->blocks[i + 1] - 1; j++)
		{
			for (int k = a->blocks[i + 1] - 1; k > j; k--)
			{
				if (a->posX[k - 1] > a->posX[k])
				{
					SwapParts(a, k - 1, k);
				}
				else if (a->posX[k - 1] == a->posX[k])
				{
					if (a->posY[k - 1] > a->posY[k])
						SwapParts(a, k - 1, k);
				}
			}
		}



	}
}
uint64_t BuildBlocksArr(AtomsSquares * s)
{
	
	for (int i = 0; i < BLOCKCOUNT*BLOCKCOUNT + 1; ++i) 	
		s->blocks[i] = 0;
	//4n
	for (int i = 0; i < s->n; ++i)
		s->blocks[CalcSquare(s, i)] += 1;


	for (int i = 1; i < BLOCKCOUNT*BLOCKCOUNT + 1; ++i)
		s->blocks[i] += s->blocks[i - 1];

	//4n
	for (int i = 0; i < s->n; ++i)
	{
		int blockNum = CalcSquare(s, i); 
		s->blocks[blockNum] -= 1;
		int newPosition = s->blocks[blockNum];

		s->copyPosX[newPosition] = s->posX[i];
		s->copyPosY[newPosition] = s->posY[i];
		s->copyPrevPosX[newPosition] = s->prevPosX[i];
		s->copyPrevPosY[newPosition] = s->prevPosY[i];
	}

	std::swap(s->posX, s->copyPosX);
	std::swap(s->posY, s->copyPosY);
	std::swap(s->prevPosX, s->copyPrevPosX);
	std::swap(s->prevPosY, s->copyPrevPosY);	
	//8n
	return 8 * s->n;
}
void Swap(double * a, double * b)
{
	double * t;
	t = a;
	a = b;
	b = t;

}
void CalcBlockSize(AtomsSquares * a, int blockCount)
{
	a->blockHeight = (a->ymax - a->ymin) / blockCount;
	a->blockWidth = (a->xmax - a->xmin) / blockCount;
}


uint64_t UpdateForce(AtomsSquares * s, int currentBlock, int currentAtom)
{
	s->forceX[currentAtom] = 0.0;
	s->forceY[currentAtom] = 0.0;

	int blockX = currentBlock % BLOCKCOUNT;
	int blockY = currentBlock / BLOCKCOUNT;
	uint64_t count = 0;
	count += CalcForceWithNeighbors(s, currentAtom, blockX - 1, blockY - 1);
	count += CalcForceWithNeighbors(s, currentAtom, blockX - 1, blockY);
	count += CalcForceWithNeighbors(s, currentAtom, blockX - 1, blockY + 1);
	count += CalcForceWithNeighbors(s, currentAtom, blockX, blockY - 1);
	count += CalcForceWithNeighbors(s, currentAtom, blockX, blockY);
	count += CalcForceWithNeighbors(s, currentAtom, blockX, blockY + 1);
	count += CalcForceWithNeighbors(s, currentAtom, blockX + 1, blockY - 1);
	count += CalcForceWithNeighbors(s, currentAtom, blockX + 1, blockY);
	count += CalcForceWithNeighbors(s, currentAtom, blockX + 1, blockY + 1);
	return count;

}

uint64_t EulerStep(AtomsSquares * s, int currentAtom)
{
	s->prevPosX[currentAtom] = s->posX[currentAtom];
	s->prevPosY[currentAtom] = s->posY[currentAtom];

	s->nextPosX[currentAtom] = s->posX[currentAtom] +
		(s->velX[currentAtom] + s->forceX[currentAtom] * s->deltaTime) *
		s->deltaTime;
	s->nextPosY[currentAtom] = s->posY[currentAtom] +
		(s->velY[currentAtom] + s->forceY[currentAtom] * s->deltaTime) *
		s->deltaTime;
	return 8;
}
//10 operations
uint64_t VerleStep(AtomsSquares * s, int currentAtom)
{
	s->nextPosX[currentAtom] = 2 * s->posX[currentAtom] - s->prevPosX[currentAtom] +
		s->forceX[currentAtom] * s->deltaTime * s->deltaTime;
	s->nextPosY[currentAtom] = 2 * s->posY[currentAtom] - s->prevPosY[currentAtom] +
		s->forceY[currentAtom] * s->deltaTime * s->deltaTime;
	return 10;
}


uint64_t NextStep(AtomsSquares * s)
{
	uint64_t count = 0;
	count+=BuildBlocksArr(s); //done
	#pragma omp parallel for reduction(+:count)
	for (int currentBlock = 0; currentBlock < BLOCKCOUNT*BLOCKCOUNT; ++currentBlock)
	{
		for (int currentAtom = s->blocks[currentBlock]; currentAtom < s->blocks[currentBlock + 1]; ++currentAtom)
		{
			count+=UpdateForce(s, currentBlock, currentAtom); //done
			if (s->iter == 0)
				count+=EulerStep(s, currentAtom);
			else
				count+=VerleStep(s, currentAtom);
		}
	}

	double *tempX = s->prevPosX;
	double *tempY = s->prevPosY;
	s->prevPosX = s->posX;
	s->prevPosY = s->posY;
	s->posX = s->nextPosX;
	s->posY = s->nextPosY;
	s->nextPosX = tempX;
	s->nextPosY = tempY;

	// �������� �� ������ �� �������
	for (int i = 0; i < s->n; ++i)
	{
		while (s->posX[i] < s->xmin)
		{
			s->prevPosX[i] += s->width;
			s->posX[i] += s->width;
			count += 2;
		}
		while (s->posX[i] > s->xmax)
		{
			s->prevPosX[i] -= s->width;
			s->posX[i] -= s->width;
			count += 2;
		}
		while (s->posY[i] < s->ymin)
		{
			s->prevPosY[i] += s->height;
			s->posY[i] += s->height;
			count += 2;
		}
		while (s->posY[i] > s->ymax)
		{
			s->prevPosY[i] -= s->height;
			s->posY[i] -= s->height;
			count += 2;
		}
	}

	s->currentTime += s->deltaTime;
	s->iter++;

	return count;
}

double calculateNeighborOffset(const AtomsSquares *s, int blockCoordinate)
{
	if (blockCoordinate < 0) return -1;
	if (blockCoordinate >= BLOCKCOUNT) return 1;
	else return 0;
}


uint64_t CalcForceWithNeighbors(const AtomsSquares *s, int currentAtom,  int nBlockX, int nBlockY)
{
	int neighborhoodBlock = ((nBlockX + BLOCKCOUNT) % (BLOCKCOUNT)) + BLOCKCOUNT * ((nBlockY + BLOCKCOUNT) % BLOCKCOUNT);
	int count = 6;
	//6op
	double offsetX = calculateNeighborOffset(s, nBlockX) * s->width;
	double offsetY = calculateNeighborOffset(s, nBlockY) * s->height;
	
	//(s->blocks[neighborhoodBlock + 1] - s->blocks[neighborhoodBlock]) * (36)
	for (int otherAtom = s->blocks[neighborhoodBlock]; otherAtom < s->blocks[neighborhoodBlock + 1]; ++otherAtom)
	{
		if (currentAtom == otherAtom) continue;
		double rx = s->posX[otherAtom] + offsetX - s->posX[currentAtom];
		double ry = s->posY[otherAtom] + offsetY - s->posY[currentAtom];
		double r = sqrt(rx * rx + ry * ry);
		double force = -1 * 4 * s->epsilon * (pow(s->sigma / r, 12) - pow(s->sigma / r, 6));
		s->forceX[currentAtom] += force * (rx / r);
		s->forceY[currentAtom] += force * (ry / r);
	}
	count += (s->blocks[neighborhoodBlock + 1] - s->blocks[neighborhoodBlock]) * (36);
	return count;
}