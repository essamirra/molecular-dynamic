#include "Atoms.h"
#include <cstdlib>
#include "math.h"
#include <stdio.h>

void NextStep(AtomsArr * arr)
{
		
	//#pragma omp parallel for
	for (int i = 0; i < arr->n; i++)
	{
		Vector2d r, newForce;
		double rAbs, force;
		arr->forceX[i] = 0;
		arr->forceY[i] = 0;
		for (int j = 0; j <i; j++)
		{
			int k = arr->iter;
			for (double left = -arr->width; left <= arr->width; left += arr->width)
				for (double bottom = -arr->height; bottom <= arr->height; bottom += arr->height)
				{
					
					r.x = arr->posX[i] - arr->posX[j]- left;
					r.y = arr->posY[i] - arr->posY[j]- bottom;
					rAbs = sqrt((r.x*r.x + r.y*r.y));
					force = 4*arr->epsilon*(pow(arr->sigma / rAbs, 12)
						- pow(arr->sigma / rAbs,6));
					arr->forceX[i] += force * (r.x / rAbs);
					arr->forceY[i] += force * (r.y / rAbs);
				}			
		}	
		for (int j = i+1; j <arr-> n; j++)
		{
			int k = arr->iter;
			for (double left = -arr->width; left <= arr->width; left += arr->width)
				for (double bottom = -arr->height; bottom <= arr->height; bottom += arr->height)
				{
					
					r.x = arr->posX[i] - arr->posX[j]- left;
					r.y = arr->posY[i] - arr->posY[j]- bottom;
					rAbs = sqrt((r.x*r.x + r.y*r.y));
					force = 4*arr->epsilon*(pow(arr->sigma / rAbs, 12)
						- pow(arr->sigma / rAbs,6));
					arr->forceX[i] += force * (r.x / rAbs);
					arr->forceY[i] += force * (r.y / rAbs);
				}			
		}	
		if (arr->iter == 0)
		{
			arr->nextPosX[i] = arr->posX[i];
			arr->nextPosY[i] = arr->posY[i];
			arr->velX[i] += arr->forceX[i] * arr->deltaTime;
			arr->velY[i] += arr->forceY[i] * arr->deltaTime;
			double t = arr->velX[i];
			double u = arr->velY[i];
			arr->nextPosX[i] += arr->velX[i] * arr->deltaTime;
			arr->nextPosY[i] += arr->velY[i] * arr->deltaTime;
			double d = arr->nextPosX[i];
			double v = arr->nextPosY[i];
		}
		else
		{
			double x, y;
			x = 2 * arr->posX[i] - arr->prevPosX[i] + arr->forceX[i] * arr->deltaTime* arr->deltaTime;
			y = 2 * arr->posY[i] - arr->prevPosY[i] + arr->forceY[i] * arr->deltaTime* arr->deltaTime;
			arr->nextPosX[i] = x;
			arr->nextPosY[i] = y;
			
		}

	}			

	for (int i = 0; i < arr->n; i++)
	{
		while(arr->nextPosX[i] < arr->xmin)
		{
			arr->nextPosX[i] += arr->width;
			arr->posX[i]+=arr->width;
		}
		while(arr->nextPosX[i] > arr->xmax)
		{
			arr->nextPosX[i] -= arr->width;
			arr->posX[i] -= arr->width;
		}
		while(arr->nextPosY[i] < arr->ymin)
		{
			arr->nextPosY[i] += arr->height;
			arr->posY[i] += arr->height;
		}
		while(arr->nextPosY[i] > arr->ymax)
		{
			arr->nextPosY[i] -= arr->height;
			arr->posY[i] -= arr->height;
		}
	}

	double *tempX = arr->prevPosX;
	double *tempY = arr->prevPosY;
	arr->prevPosX = arr->posX;
	arr->prevPosY = arr->posY;
	arr->posX = arr->nextPosX;
	arr->posY = arr->nextPosY;
	arr->nextPosX = tempX;
	arr->nextPosY = tempY;

	arr->iter++;
}

void InitAtomsArr(AtomsArr * n, const char * fileName)
{
	FILE *file = fopen(fileName, "r");
	if (file == NULL)
	{
		printf("ERROR: Can't open file %s", fileName);
		return;
	}
	fscanf(file, "N=%u\r\n", &n->n);
	n->posX = new double[n->n];
	n->posY = new double[n->n];
	n->prevPosX = new double[n->n];
	n->prevPosY = new double[n->n];
	n->nextPosX = new double[n->n];
	n->nextPosY = new double[n->n];
	n->velX = new double[n->n];
	n->velY = new double[n->n];
	n->forceX = new double[n->n];
	n->forceY = new double[n->n];
	fscanf(file, "SIGMA=%lf\r\n", &n->sigma);
	fscanf(file, "EPSILON=%lf\r\n", &n->epsilon);
	fscanf(file, "DT=%lf\r\n", &n->deltaTime);
	fscanf(file, "T=%lf\r\n", &n->currentTime);
	fscanf(file, "XMIN=%lf\r\n", &n->xmin);
	fscanf(file, "XMAX=%lf\r\n", &n->xmax);
	fscanf(file, "YMIN=%lf\r\n", &n->ymin);
	fscanf(file, "YMAX=%lf\r\n", &n->ymax);
	n->height = n->ymax - n->ymin;
	n->width = n->xmax - n->xmin;
	fscanf(file, "ITERATIONS=%u\r\n", &n->maxIteration);
	n->iter = 0;
	for (int i = 0; i < n->n; ++i)
		fscanf(file, "%lf%lf%lf%lf", &n->posX[i], &n->posY[i], &n->velX[i], &n->velY[i]);
	fclose(file);
}

 Vector2d Atom_getVelocity(Atom atom, double delta_t)
{
	Vector2d result;
	result.x = (atom.position.x - atom.previousPosition.x) / delta_t;
	result.y = (atom.position.y - atom.previousPosition.y) / delta_t;
	return result;
	
}

void SaveAtomsArr(AtomsArr* s, const char *fileName)
{
	

	FILE *file = fopen(fileName, "w+");
	if (file == NULL)
	{
		printf("ERROR: Can't create file %s", fileName);
		return;
	}

	fprintf(file, "N=%u\n", s->n);
	fprintf(file, "SIGMA=%lf\n", s->sigma);
	fprintf(file, "EPSILON=%lf\n", s->epsilon);
	fprintf(file, "DT=%lf\n", s->deltaTime);
	fprintf(file, "T=%lf\n", s->currentTime);
	fprintf(file, "XMIN=%lf\n", s->xmin);
	fprintf(file, "XMAX=%lf\n", s->xmax);
	fprintf(file, "YMIN=%lf\n", s->ymin);
	fprintf(file, "YMAX=%lf\n", s->ymax);
	fprintf(file, "ITERATIONS=%u\n", s->maxIteration);

	for (int i = 0; i < s->n; ++i)
	{
		Vector2d result;
		result.x = (s->posX[i] - s->prevPosX[i]) / s->deltaTime;
		result.y = (s->posY[i] - s->prevPosY[i]) / s->deltaTime;
		
		fprintf(file, "%10.6lf %10.6lf %10.6lf %10.6lf\n", s->posX[i], s->posY[i], result.x, result.y);
	}

	fclose(file);
}