#pragma once
#include "Vector.h"


typedef struct
{
	Vector2d position;
	Vector2d previousPosition;
	Vector2d velocity;
	Vector2d force;	
}Atom;

typedef struct
{

	double sigma;
	double epsilon;
	double deltaTime;
	double currentTime;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double width;
	double height;
	unsigned maxIteration;
	int iter;
	int n;
	double * posX;
	double * posY;
	double * prevPosX;
	double * prevPosY;
	double * nextPosX;
	double * nextPosY;
	double * velX;
	double * velY;
	double * forceX;
	double * forceY;

//	Atom * atoms;
}AtomsArr;


AtomsArr * CreateAtomsArr(int n);
void InitAtomsArr(AtomsArr * n, const char * fileName);
void InitAtomsArr(AtomsArr * atoms_arr);
void UpdateForce(AtomsArr * atoms_arr);
void NextStep(AtomsArr * arr);
void SaveAtomsArr(AtomsArr* s, const char *fileName);