#pragma once
#include <cstdint>

typedef struct
{

    int rank;
    int proccesNum;
    

   

    int rankOfMyUpper;
    int rankOfMyLower;
    int rankOfMyLeft;
    int rankOfMyRight;

    double myXmin;
    double myYmin;
    double myXmax;
    double myYmax;
    int blocksInEdgeForProccess;

    double sigma;
    double epsilon;
    double deltaTime;
    double currentTime;

    double width;
    double height;
    unsigned maxIteration;
    int iter;
    int n;

    double * posX;
    double * posY;
    double * prevPosX;
    double * prevPosY;
    double * nextPosX;
    double * nextPosY;
    double * velX;
    double * velY;
    double * forceX;
    double * forceY;

    int * blocks;
    double blockWidth;
    double blockHeight;
    int blocksInEdge;
    

    double * copyPosX;
    double * copyPosY;
    double * copyPrevPosX;
    double * copyPrevPosY;

    double xmin;
    double xmax;
    double ymin;
    double ymax;

}AtomsSquaresMPI;

void Init(AtomsSquaresMPI * s, const char * fileName, int blocksInEdge);
void InitSystemParameters(AtomsSquaresMPI * s, const char * fileName, int blocksInEdge);
void AllocSystemMem(AtomsSquaresMPI * s);
void InitMPISystemParameters(AtomsSquaresMPI * s);
void ReadAtomsFromFile(AtomsSquaresMPI * s, const char * fileName);
void BuidBlocksSystem(AtomsSquaresMPI  * s);
void UpdateProccesAtoms(AtomsSquaresMPI * s);
void NextStep(AtomsSquaresMPI * s);
void SendAndRecieveFlyers(AtomsSquaresMPI * s);
void BuildBlocksSystemForProccessAtoms(AtomsSquaresMPI * s);
void SendAndRecieveEdges(AtomsSquaresMPI * s);
void UpdateForce(AtomsSquaresMPI * s, int currentBlock, int currentAtom);
void UpdatePositions(AtomsSquaresMPI * s);
void EulerStep(AtomsSquaresMPI * s, int currentAtom);
void VerleStep(AtomsSquaresMPI * s, int currentAtom);
void CalcForceWithNeighbors(const AtomsSquaresMPI *s, int currentAtom, int nBlockX,
    int nBlockY);
uint64_t BuildBlocksArr(AtomsSquaresMPI * s);
int CalcSquareMPI(AtomsSquaresMPI* a, int id);