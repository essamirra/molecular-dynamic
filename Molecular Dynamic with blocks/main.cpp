#include "Atoms.h"
#include <omp.h>
#include <iostream>
#include "AtomsSquares.h"
#define BLOCKCOUNT 2
using namespace std;
int main()
{
	AtomsSquares t;
	Init(&t, "input.txt");
	double start = omp_get_wtime();
	CalcBlockSize(&t, BLOCKCOUNT);
	//BuildBlocksArr(&t);
	uint64_t flop = 0;
	for(int i = 0; i < t.maxIteration; i++)
	{
		flop += NextStep(&t);		
			
	}
	double end = omp_get_wtime();
	cout << "Flop: " << flop << endl;
	cout << "GFlops: " << fixed << flop / (end - start) / 1000000000 << endl;
	cout<<end-start << endl;
	Save(&t, "13. Pronina 4 100000.txt");
	SaveDistances(&t);
	return 0;
}
